package src;

public class Task4 {
    public static int calculateSum() {
        int sum = 0;
        for (int i = 1; i <= 9; i += 2) {
            sum += factorial(i);
        }

        return sum;
    }

    static int factorial(int value) {
        if (value == 0) {
            return 1;
        }

        int mul = 1;
        for (int i = value; i > 0; i--) {
            mul *= i;
        }

        return mul;
    }
}
