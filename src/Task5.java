package src;

public class Task5 {
    public static int secondMax(int[] array) {
        int first = Integer.MIN_VALUE;
        int second = Integer.MIN_VALUE;

        if (array.length == 1) {
            return array[0];
        }

        for (int element : array) {
            if (first <= element) {
                second = first;
                first = element;
            }
        }

        return second;
    }
}
