package src;

public class Task3 {
    public static int scm(int a, int b, int c) {
        int max = Integer.max(Integer.max(a, b), c);

        for (int i = max; i < Integer.MAX_VALUE; i++) {
            if (i % a == 0 && i % b == 0 && i % c == 0) {
                return i;
            }
        }

        return -1;
    }
}