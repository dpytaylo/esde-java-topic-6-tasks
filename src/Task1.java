package src;

public class Task1 {
    public static void gcdAndScm(int a, int b) {
        int max = Integer.max(a, b);

        for (int i = max; i < Integer.MAX_VALUE; i++) {
            if (i % a == 0 && i % b == 0) {
                System.out.println("1: GCD = " + (a * b) / i);
                System.out.println("   SCM = " + i);
                break;
            }
        }
    }
}
