package src;

public class Main {
    public static void main(String[] args) {
        Task1.gcdAndScm(10, 15);
        System.out.println("2: " + Task2.gcd(2, 3, 4, 5));
        System.out.println("3: " + Task3.scm(1, 2, 3));
        System.out.println("4: " + Task4.calculateSum());
        System.out.println("5: " + Task5.secondMax(new int[] {1, 2, 5, 8, 3, 2, 4}));
    }
}
