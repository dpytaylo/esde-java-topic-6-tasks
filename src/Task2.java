package src;

public class Task2 {
    public static int gcd(int a, int b, int c, int d) {
        int min = Integer.min(Integer.min(a, b), Integer.min(c, d));

        for (int i = min; i > 0; i--) {
            if (a % i == 0 && b % i == 0 && c % i == 0 && d % i == 0) {
                return i;
            }
        }

        return -1;
    }
}
